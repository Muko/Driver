from flask import Flask ,render_template, url_for, session, redirect, request
from flask_socketio import SocketIO, send,emit
from threading import Thread
from time import sleep
import serial, itertools
import socket , json
import eventlet
import eventlet.debug
import os


eventlet.monkey_patch()
async_mode = 'eventlet'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'spiecret'
socketio = SocketIO(app,async_mode=async_mode)
eventlet.debug.hub_prevent_multiple_readers(False)
#s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#s.bind(('0.0.0.0', 8879))


#ser=serial.Serial("/dev/tty.SLAB_USBtoUART",115200, timeout=10)
#while 1:
#    data = ser.readline()
#    print(data)

@app.route('/')
def index():
    return render_template('demo.html')


@socketio.on('message')
def handleMessage(msg):


    if (msg=='recv' or msg=='conn'):
        while True:

            ##UDP測試版
        #    msg1 = s.recv(1024)
        #    msg2=json.loads(msg1)
        #    emit('message',msg2)

            ##Uart
        #    data = ser.readline()
        #    msg2=json.loads(data)
        #    emit('message',msg2)

            with open('test.txt', 'r') as f:
                x = f.readlines()
            f.close()

            for element in itertools.cycle(x):
                msg=eval(element)
                msg1=json.dumps(msg)
                msg2=json.loads(msg1)
                emit('message',msg2)
                sleep(0.5)
                print(msg2)
            return url_for('index')
        #ser.close()


if __name__ == '__main__':
 socketio.run(app,host='0.0.0.0',port=8877)

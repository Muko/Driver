{
"gauge": {
"enabled": true,
"title": {
"enabled": false,
"margin": {
"left": 0,
"top": 0,
"bottom": 10,
"right": 0
},
"padding": {
"left": 0,
"top": 0,
"bottom": 0,
"right": 0
}
},
"margin": {
"left": 30,
"top": 30,
"bottom": 30,
"right": 30
},
"padding": {
"left": 0,
"top": 0,
"bottom": 0,
"right": 0
},
"chartLabels": [
{
"zIndex": 50,
"enabled": true,
"fontSize": "12px",
"fontFamily": "'Verdana', Helvetica, Arial, sans-serif",
"fontColor": "#212121",
"fontOpacity": 1,
"fontDecoration": "none",
"fontStyle": "normal",
"fontVariant": "normal",
"fontWeight": "normal",
"letterSpacing": "normal",
"textDirection": "ltr",
"lineHeight": "normal",
"textIndent": 0,
"vAlign": "top",
"hAlign": "center",
"wordWrap": "normal",
"wordBreak": "normal",
"textOverflow": "",
"selectable": false,
"disablePointerEvents": false,
"useHtml": true,
"text": "<span style=\"color: #64B5F6; font-size: 13px\">Wind Direction: </span><span style=\"color: #5AA3DD; font-size: 15px\">120° (+/- 0.5°)</span><br><span style=\"color: #1976d2; font-size: 13px\">Wind Speed:</span> <span style=\"color: #166ABD; font-size: 15px\">12m/s</span>",
"position": "left-top",
"width": null,
"height": null,
"anchor": "center-top",
"offsetX": 0,
"offsetY": -20,
"rotation": 0,
"adjustFontSize": {
"width": false,
"height": false
},
"minFontSize": 8,
"maxFontSize": 72,
"background": {
"zIndex": 0,
"enabled": true,
"fill": "#fff",
"stroke": {
"color": "#E0F0FD",
"thickness": 1
},
"disablePointerEvents": false,
"cornerType": "round",
"corners": 0
},
"padding": {
"left": 20,
"top": 15,
"bottom": 15,
"right": 20
}
}
],
"credits": {
"enabled": false
},
"type": "circular-gauge",
"fill": "#fff",
"stroke": "none",
"data": [
120,
12
],
"cap": {
"enabled": true,
"fill": "#1976d2",
"stroke": "none",
"radius": "4%"
},
"axes": [
{
"enabled": true,
"scale": {
"type": "linear",
"inverted": false,
"maximum": 360,
"minimum": 0,
"minimumGap": 0.1,
"maximumGap": 0.1,
"softMinimum": null,
"softMaximum": null,
"alignMinimum": true,
"alignMaximum": true,
"maxTicksCount": 1000,
"ticks": {
"mode": "linear",
"base": 0,
"allowFractional": true,
"interval": 30
},
"minorTicks": {
"mode": "linear",
"base": 0,
"allowFractional": true,
"interval": 10
},
"stackMode": "none",
"stackDirection": "direct",
"stickToZero": true,
"comparisonMode": "none"
},
"ticks": {
"zIndex": 10,
"enabled": true,
"length": "4%",
"fill": "#7c868e",
"position": "outside"
},
"minorTicks": {
"zIndex": 10,
"enabled": false
},
"labels": {
"zIndex": 10.00004,
"enabled": true,
"padding": {
"left": 3,
"top": 3,
"bottom": 3,
"right": 3
},
"format": "{%Value}°",
"position": "outside"
},
"minorLabels": {
"zIndex": 10.00004,
"enabled": false
},
"startAngle": 0,
"sweepAngle": -360,
"width": "1%",
"fill": "#7c868e"
},
{
"enabled": true,
"scale": {
"type": "linear",
"inverted": false,
"maximum": 25,
"minimum": 0,
"minimumGap": 0.1,
"maximumGap": 0.1,
"softMinimum": null,
"softMaximum": null,
"alignMinimum": true,
"alignMaximum": true,
"maxTicksCount": 1000,
"ticks": {
"mode": "linear",
"base": 0,
"allowFractional": true,
"interval": 5
},
"minorTicks": {
"mode": "linear",
"base": 0,
"allowFractional": true,
"interval": 1
},
"stackMode": "none",
"stackDirection": "direct",
"stickToZero": true,
"comparisonMode": "none"
},
"ticks": {
"zIndex": 10,
"enabled": true,
"length": "4%",
"fill": "#7c868e",
"position": "outside"
},
"minorTicks": {
"zIndex": 10,
"enabled": false
},
"labels": {
"zIndex": 10.00004,
"enabled": true,
"padding": {
"left": 3,
"top": 3,
"bottom": 3,
"right": 3
},
"format": "{%Value} m/s",
"position": "outside"
},
"minorLabels": {
"zIndex": 10.00004,
"enabled": false
},
"startAngle": 270,
"sweepAngle": 180,
"width": "1%",
"radius": "40%",
"fill": "#7c868e"
}
],
"pointers": [
{
"zIndex": 120,
"enabled": true,
"pointerType": "marker",
"fill": "#64b5f6",
"stroke": "none",
"size": "15%",
"radius": "97%"
},
{
"zIndex": 40.0001,
"enabled": true,
"pointerType": "needle",
"fill": "#1976d2",
"stroke": "none",
"axisIndex": 1,
"startWidth": "2%",
"startRadius": "6%",
"endWidth": "0%",
"endRadius": "38%"
}
]
}
}

import itertools
import socket,os,sys,tty,termios,json,time,random
import eventlet
eventlet.monkey_patch()
from time import sleep
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

with open('test.txt', 'r') as f:
    x = f.readlines()
f.close()

for element in itertools.cycle(x):
    msg=eval(element)
    j = json.dumps(msg)
    s.sendto( j.encode(),('172.20.10.3', 8879))
    sleep(0.05)
s.close()
